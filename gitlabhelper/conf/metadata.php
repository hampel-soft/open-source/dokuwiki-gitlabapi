<?php
/*
 * Gitlabissues plugin, configuration metadata
 *
 */


$meta['gitlab_url'] = array('string');
$meta['api_token'] = array('string');
$meta['release_url'] = array('string');
$meta['release_remote'] = array('onoff');
$meta['remote_pubkey'] = array('setting');
$meta['release_path'] = array('string');
$meta['use_namespace'] = array('onoff');
