<?php
/*
 * Gitlabissues plugin, default configuration settings
 *
 */

$conf['gitlab_url'] = 'https://gitlab.com/api/v3';
$conf['api_token'] = '';
$conf['release_url'] = '';
$conf['release_remote'] = 0;
$conf['remote_pubkey'] = '';
$conf['release_path'] = '';
$conf['use_namespace'] = 0;
