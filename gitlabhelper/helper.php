<?php
/**
 * DokuWiki Plugin gitlabhelper (Helper Plugin)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Joerg Hampel <joerg@hampel.at>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

class helper_plugin_gitlabhelper extends DokuWiki_Plugin {


	public function getMethods() {
	    $result = array();
	    $result[] = array(
	        'name' => 'getGitlabObject',
	        'desc' => 'Gets a list of elements from gitlab',
	        'params' => array(
	            'type' => 'string',
	            'projectid (optional)' => 'string',
	            'issueid (optional)' => 'string'
	        ),
	        'return' => array('data' => 'array'),
	    );
	    $result[] = array(
	        'name' => 'createNewIssue',
	        'desc' => 'Creates a new issue',
	        'params' => array(
	            'projectid' => 'string',
	            'title' => 'string',
	            'description' => 'string'
	        ),
	        'return' => array('result' => 'string'),
	    );
	    $result[] = array(
	        'name' => 'createNewComment',
	        'desc' => 'Creates a new comment',
	        'params' => array(
	            'projectid' => 'string',
	            'issueid' => 'string',
	            'issuebody' => 'string'
	        ),
	        'return' => array('result' => 'string'),
	    );
	    $result[] = array(
	        'name' => 'cleanDate',
	        'desc' => 'Formats API date into human readable date',
	        'params' => array(
	            'API' => 'string'
	        ),
	        'return' => array('human' => 'string'),
	    );
	    $result[] = array(
	        'name' => 'formatText',
	        'desc' => 'Formats Text into HTML',
	        'params' => array(
	            'text' => 'string'
	        ),
	        'return' => array('output' => 'string'),
	    );
	    $result[] = array(
	        'name' => 'formatFilesize',
	        'desc' => 'Formats bytes into kBytes etc.',
	        'params' => array(
	            'size' => 'integer',
	            'bytes' => 'integer'
	        ),
	        'return' => array('output' => 'string'),
	    );
	    $result[] = array(
	        'name' => 'getReleaseFiles',
	        'desc' => 'Reads all available files for one release from directory',
	        'params' => array(
	            'projectpath' => 'string'
	        ),
	        'return' => array('result' => 'array'),
	    );
	    // and more supported methods...
	    return $result;
	}


	/**
	 * Gets a list of elements from gitlab
	 *
	 * @param $type
	 * @param $project-id
	 * @param $issue-id
	 * @return string
	 */
	function getGitlabObject($type, $projectid = null, $issueid = null) {
		// get private token from config
		$privtoken = $this->getConf('api_token');

	    //get all notes
	    $pageurl = $this->getConf('gitlab_url');
	
	    switch($type) {
			// GET /projects
			case "projects":
				$pageurl .= '/projects?page=1&per_page=100&private_token=' . $privtoken;
				break;
			// GET /project
			// Get a specific project, identified by project ID or NAMESPACE/PROJECT_NAME, 
			// which is owned by the authenticated user. If using namespaced projects call 
			// make sure that the NAMESPACE/PROJECT_NAME is URL-encoded, 
			// eg. /api/v3/projects/diaspora%2Fdiaspora (where / is represented by %2F).
			case "project":
				$pageurl .= '/projects/' . urlencode($projectid) . '?private_token=' . $privtoken;
				// returns "path_with_namespace": "diaspora/diaspora-project-site"
				break;
			// GET /projects/:id/repository/tags
			case "tags":
				$pageurl .= '/projects/' . $projectid . '/repository/tags?page=1&per_page=10&private_token=' . $privtoken;
				break;
			// GET /projects/:id/repository/commits
			case "commits":
				$pageurl .= '/projects/' . $projectid . '/repository/commits?ref_name=' . $issueid . '&per_page=100&private_token=' . $privtoken;
				break;
			// GET /projects/:id/labels
			case "labels":
				$pageurl .= '/projects/' . $projectid . '/labels?page=1&per_page=100&private_token=' . $privtoken;
				break;
			// GET /projects/:id/issues
			case "issues":
				$pageurl .= '/projects/' . $projectid . '/issues?labels=external&state=opened&page=1&per_page=100&private_token=' . $privtoken;
				break;
			// GET /projects/:id/issues
			case "issuesclosed":
				$pageurl .= '/projects/' . $projectid . '/issues?labels=external&state=closed&page=1&per_page=100&private_token=' . $privtoken;
				break;
			// GET /projects/:id/issues/:issue_id/notes
			case "notes":
				$pageurl .= '/projects/' . $projectid . '/issues/' . $issueid . '/notes?page=1&per_page=100&private_token=' . $privtoken;
				break;
			default:
				return false;
	    }
	
		//get object from gitlab API
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $pageurl);
		$html = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		curl_close($ch);

		//print_r("<pre>" . $pageurl . "</pre>");
		//print_r("<pre>" . $html . "</pre>");

		//get array from html
		$json = new JSON();

		if($data = $json->decode($html)) {
			//return the json object if there is one
			return $data;
		}
		else {
			if($status != "200")
				//return an error message
				return (object) array("message" => $status . ": " . $html);
			else
				//return an empty array 
				return array();
		}
	}



	/*
	Creates a new project issue.

	POST /projects/:id/issues
	Parameters:

	id (required) - The ID of a project
	title (required) - The title of an issue
	description (optional) - The description of an issue
	assignee_id (optional) - The ID of a user to assign issue
	milestone_id (optional) - The ID of a milestone to assign issue
	labels (optional) - Comma-separated label names for an issue

	*/
	public function createNewIssue($projectid, $title, $description) {
	    // get private token from config
	    $privtoken = $this->getConf('api_token');

	    //target url
	    $pageurl = $this->getConf('gitlab_url') . '/projects/' . $projectid . '/issues';

		//set POST variables
		$fields = array(
			'id' => urlencode($projectid),
			'title' => urlencode($title),
			'description' => urlencode($description),
			'labels' => urlencode('external,dokuwiki')
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

	    //post object to gitlab API
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $pageurl);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $privtoken));
		$result = curl_exec($ch);
		curl_close($ch);

	    return $result;
	}



	/*
	Creates a new comment to an issue.

	POST /projects/:id/issues/:issue_id/notes
	Parameters:

	id (required) - The ID of a project
	issue_id (required) - The ID of an issue
	body (required) - The content of a note

	*/
	public function createNewComment($projectid, $issueid, $body) {
	    // get private token from config
	    $privtoken = $this->getConf('api_token');

	    //target url
	    $pageurl = $this->getConf('gitlab_url') . '/projects/' . $projectid . '/issues/' . $issueid . '/notes';

		//set POST variables
		$fields = array(
			'id' => urlencode($projectid),
			'issue_id' => urlencode($issueid),
			'body' => urlencode($body)
		);

		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

	    //post object to gitlab API
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $pageurl);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . $privtoken));
		$result = curl_exec($ch);
		curl_close($ch);

	    return $result;
	}



    /**
     * Format gitlab API date (2015-01-09T15:27:50.871Z) to human readable one
     *
     * @param $date
     * @return string
     */
    public function cleanDate($API) {
		$human = "";
		$human .= substr($API,  8, 2) . ".";
		$human .= substr($API,  5, 2) . ".";
		$human .= substr($API,  0, 4) . " ";
		$human .= substr($API, 11, 2) . ":";
		$human .= substr($API, 14, 2);
		return $human;
    }


    /**
     * format issue description and comments
     *
     * @param $text
     * @return string
     */
    public function formatText($text) {
		$output = $text;

		// parse image links
		$do = true;
		while ($do) {
			$startlabel = strpos($output, "![");
			if ($startlabel !== false) {
				$endlabel = strpos($output, "](", $startlabel);
				$label = substr($output, $startlabel + 2, $endlabel - $startlabel - 2);
			
				$starturl = $endlabel + 2;
				$endurl = strpos($output, ")", $starturl);
				$url = substr($output, $starturl, $endurl - $starturl);
			
				$output = substr($output, 0, $startlabel) 
				        . "<a href='" . $url . "'><i class='fa fa-file'></i> " . $label . "</a>"
				        . substr($output, $endurl + 1);
			}
			else {
				$do = false;
			}
		}


/*
		$matches = null;
		preg_match_all('/!\[(.*?)\]\((.*?)\)/', $output, $matches);
		var_dump($matches[1]);
*/

		$output = str_replace("\r\n", "\n", $output);
		$output = str_replace("\n", "<br/>", $output);

		return $output;
    }




    /**
     * format filesize
     *
     * @param $text
     * @return array
     */
	function formatFilesize($bytes, $decimals = 2) {
	  $sz = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
	  $factor = floor((strlen($bytes) - 1) / 3);
	  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . " " . @$sz[$factor];
	}



    /**
     * get all files in release directory
     *
     * @param $text
     * @return array
     */
    public function getReleaseFiles($namespace, $project, $tag_name) {
		if($this->getConf('release_remote') == '1') {
			$clear_path = $namespace . "/" . $project . "/" . $tag_name;

			$public_key = $this->getConf('remote_pubkey');
			if ( !empty($public_key) ) {
				$openssl_public_encrypt($clear_path, $crypt_path, $public_key);
				while ($msg = openssl_error_string())
				    echo $msg . "<br />\n";
				$enc_path = base64_encode($crypt_path);
			}
			else {
				$enc_path = base64_encode($clear_path);
			}

			$fullRelUrl = $this->getConf('release_url') . "?l=" . $enc_path;


			// erzeuge einen neuen cURL-Handle
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $fullRelUrl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			$curlresponse = curl_exec($ch);
			if($curlresponse === false) {
			    $result = curl_error($ch);
			}
			else {
				$resultarr = json_decode($curlresponse, true);
				if( is_array($resultarr) ) {
					foreach($resultarr as $entry) {
						$result[] = array($entry[0], $this->formatFilesize($entry[1]));
					}
				}
			}
			//$cinfo = curl_getinfo($ch);
			//print ("<pre>");
			//print_r ($cinfo);
			//print ("</pre>");
			curl_close($ch);
		} 
		else {
			$relpath = $this->getConf('release_path');
			$use_namespace = $this->getConf('use_namespace');
			$fullprojpath = $relpath . "/" . ($use_namespace == '1' ? $namespace . "/" : "") . $project;

			if ($handle = @opendir($fullprojpath)) {
			    /* Das ist der korrekte Weg, ein Verzeichnis zu durchlaufen. */
			    while (false !== ($file = readdir($handle))) {
			        if ($file != "." && $file != "..") {
						if ($size = filesize($fullprojpath . "/" . $file))
			            	$result[] = array($file, $this->formatFilesize($size));
			        }
			    }
			    closedir($handle);
			}
		}
		return $result;
	}



    /**
     * get url to directory containing release files
     *
     * @return $text
     */
    public function getReleaseUrl($namespace, $project, $tag_name, $file) {
		if($this->getConf('release_remote') == '1') {
			$clear_url = $namespace . "/" . $project . "/" . $tag_name . "/" . $file;

			$public_key = $this->getConf('remote_pubkey');
			if ( !empty($public_key) ) {
				openssl_public_encrypt($clear_url, $crypt_url, $this->getConf('remote_pubkey'));
				$enc_path = base64_encode($crypt_url);
			}
			else {
				$enc_path = base64_encode($clear_url);
			}

			$url = $this->getConf('release_url') . "?d=" . $enc_path;
		}
		else {
			$use_namespace = $this->getConf('use_namespace');
			$url = $this->getConf('release_url') . ($use_namespace == '1' ? $namespace . "/" : "") . $project . "/" . $tag_name . "/" . $file;
		}
		return $url;
	}


}
// vim:ts=4:sw=4:et:
