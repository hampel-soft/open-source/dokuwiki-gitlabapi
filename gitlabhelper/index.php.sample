<?php
/**
 * DokuWiki Plugin gitlabhelper (Helper Plugin)
 * 
 * This is the script running on a remote server if download files do not 
 * reside on the same server as the dokuwiki installation.
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Joerg Hampel <joerg@hampel.at>
 */


// ========================================================================
// Config
// ========================================================================
$title = "HAMPEL SOFTWARE ENGINEERING - Download Server";
$basepath = "/var/services/web/";
$baseurl = "/";
$basedir = "releases/";

// Optional: Load private key for decryption of url parameters
//$private_key = file_get_contents('/path/to/private/key', FILE_USE_INCLUDE_PATH);
$private_key = "";


// ========================================================================
// Get request parameters
// ========================================================================

$l_param = filter_input(INPUT_GET, 'l', FILTER_SANITIZE_SPECIAL_CHARS);
$d_param = filter_input(INPUT_GET, 'd', FILTER_SANITIZE_SPECIAL_CHARS);
$show404 = true;


// ========================================================================
// List files
// ========================================================================
if ( $l_param != "" ) {
	if ( !empty($private_key) ) {
		$crypt_path = base64_decode($l_param);
		openssl_private_decrypt($crypt_path, $path, $privatekey);
		while ($msg = openssl_error_string())
		    echo $msg . "<br />\n";
		$path = base64_decode($crypt_path);
	}
	else {
		$path = base64_decode($l_param);
	}


	// If path is valid
	if ( !empty($path) && ($path!=".") && ($path!="/") ) {

		// if handle to path is valid
		if ($handle = opendir($basepath . $basedir . $path)) {

			$files = array();

			// return all non-blacklisted files and directories 
			$blacklist = array(".", "..", "@eaDir");
			while (false !== ($file = readdir($handle))) {
				if ( !in_array($file, $blacklist) && $size = filesize($basepath . $basedir . $path . "/" . $file) ) {
					$files[] = array($file, $size);
				}
			}
			closedir($handle);

			// output a json list of files
			echo json_encode($files);

			$show404 = false;
		}
	}
}

// ========================================================================
// Download file
// ========================================================================
elseif ( $d_param != "" ) {
	//remove falsely copied parameters and equal signs
	if(strstr($d_param, 'd=', false)) {
		$d_param = str_replace("=", "", substr(strstr($d_param, 'd=', false), 2));
	}

	if ( !empty($private_key) ) {
		$crypt_url = base64_decode($d_param);
		openssl_private_decrypt($crypt_url, $clear_url, $private_key);
		while ($msg = openssl_error_string())
		    echo $msg . "<br />\n";
		$clear_url = base64_decode($crypt_url);
	}
	else {
		$clear_url = base64_decode($d_param);
	}
	$file = $basepath . $basedir . $clear_url;

	// If file is valid
	if (file_exists($file) && is_file($file)) {

	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($file).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    
		// stop output buffering if it's already started
		// https://www.php.net/manual/en/function.ob-get-level.php
		if (ob_get_level()) ob_end_clean();

		// Read the file and write it to the output buffer
		readfile($file);

		$show404 = false;
	} 
}

// ========================================================================
// Show download form
// ========================================================================
else {
	echo '<html><head><title>';
	echo $title;
	echo '</title></head><body style="text-align:center">';
	echo '<form action="' . $baseurl . $basedir . '" method="GET">';
	echo '<label for="d">Please enter the download ID and press the button</label><br/>';
	echo '<input type="text" name="d" size="100" value="" /><br/>';
	echo '<input type="submit" value="Download now!"/>';
	echo '</form>';
	echo '</body></html>';
	$show404 = false;
}

// ========================================================================
// Show 404 error message 
// ========================================================================
if ($show404 == true) {
	header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found"); 
	echo "error 404 - file not found";
}

exit;

?>
